# SSHD-SFTP

Cette image permet de démarrer un service SSH (ou SFTP only avec chroot). 

Elle est utilisée pour pallier le manque, dans certaines applications, d'un composant permettant de manipuler des fichiers côté serveur d'application.


Image de base : Alpine + bash


Deux modes de fonctionnement :

- Si un fichier de login/password est présenté (**/root/user-accounts**), on crée N comptes utilisateurs,

- sinon, on crée un seul compte, défini par les variables SSHD_USER_NAME et associées. Celui-ci a alors la possibilité d'être déclaré sudoer.


## Fonctionnement

Par défaut, sans variable d'environnement ni secret, cela ne démarrera pas.

Il faut à minima passer la variable SSHD_AUTHORIZED_PUBLIC_KEYS ou la variable SSHD_USER_PASSWORD (à laquelle, en environnement Swarm, il faut préférer un secret).



## Entrypoint / Command

Le processus d'avant plan est "Supervisord", et lance lui-même sshd, pour permettre facilement les surcharges de cette image (fail2ban / sshguard).


## Secret

Dans le cas (par défaut) où un seul utilisateur est prévu, son mot de passe peut être fourni via un secret Docker.

Le nom de ce secret est paramétrable par la variable SSHD_SECRET_KEY.

Sa valeur par défaut est "SSHD_USER_PASSWORD_SECRET_NAME".


## Fichier de comptes multiples

On peut monter un fichier énumérant la liste des comptes à créer (**/root/user-accounts**).

Son format est le suivant :

```
usera password
userb pubkey:ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBMiqwt9Qel0E/BbseinIGDGIYSO6TpD0Fzr7ihO6AjSQYIpBvZdGbkzAO20kZm1u/DILOXPdNu+VlkCuxKWCltc= ma-grande-clé
userc pubkey:ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBDcYs9YI696HVLXu5VHsFDSMZCr9I781GTtfr2zezR/ ma-petite-clé
...
```
Le format des clés est libre (rsa, ecdsa, ed25519). 

Elles doivent juste être préfixées par "pubkey:".

Le dernier champ (commentaire, peut être omis).

## Variables d'environnement 

- SSHD_DEBUG_EP (false) : ajout d'informations sur la config au démarrage

- SSHD_PORT (22) : port d'écoute du serveur dans le conteneur.

- SSHD_USER_UID (1000) : UID de l'utilisateur à créer. C'est aussi l'UID de tous les utilisateurs dans le cas où un fichier "user-accounts" **est fourni** et que la variable SSHD_SAME_UID vaut true.

- SSHD_USER_GID (1000) : GID de l'utilisateur (des utilisateurs) à créer (et GID du groupe à créer)

- SSHD_SAME_UID (false) : utiliser le même UID pour tout le monde. Si cette variable est à "false", et uniquement dans le cas où un fichier de comptes multiples user-accounts **est fourni**, alors chaque utilisateur a son UID.

- SSHD_GROUP_NAME (scpusers) : nom du groupe (créé à la volée) de l'utilisateur (des utilisateurs)

- SSHD_LOGLEVEL (info) : niveau de log du serveur SSH (les logs sont envoyés en sortie standard)

- SSHD_STRICT_MODES (yes) : valeur de la directive StrictModes dans sshd_config.


- SSHD_MAX_LIFETIME (0) : durée du vie du service. Il s'arrête tout seul au bout du temps spécifié. Format : 0 (illimité), 10s, 5m, 2h, 1d ...

### Variables concernant SFTP

**Attention** : on active soit SSHD_SFTP_ENABLE, soit SSHD_SFTP_ONLY

- SSHD_SFTP_ENABLE (false) : activer sftp, le sous-système utilisé est **/usr/lib/ssh/sftp-server**, l'utilisateur n'est pas chrooté, il n'y a pas de création du répertoire d'upload, le propriétaire du répertoire monté dans /home est libre.

- SSHD_SFTP_ONLY (false) : activer sftp et limiter à sftp, **avec chroot**. Le sous-système utilisé est **internal-sftp**, l'utilisateur est chrooté, on est sensible à la création du répertoire d'upload, le propriétaire du répertoire monté dans /home est forcément `root.root` (c'est imposé par sshd).

- SSHD_SFTP_ONLY_CHROOT_DIRECTORY : nom du dossier de chroot. Ceci permet de choisir un même dossier pour N utilisateurs. Chemin absolu uniquement (ex: /data). Si cette variable est vide, on chroote sur $HOME. 

- SSHD_SFTP_UPLOAD_DIRNAME (upload) : nom du dossier (sous dossier du chroot) dans lequel l'upload est autorisé. Si cette variable est absente ou vide, aucun upload n'est possible.


Deux cas de figuer existent donc :

  - Si on choisit un dossier unique pour le CHROOT (SSHD_SFTP_ONLY_CHROOT_DIRECTORY non vide), alors SSHD_SFTP_UPLOAD_DIRNAME est implicite et vaut "upload". 
  
  - Si SSHD_SFTP_ONLY_CHROOT_DIRECTORY est vide, alors si SSHD_SFTP_UPLOAD_DIRNAME est optionelle et sa présence rend l'upload possible et engendre la création du dossier qu'elle nomme à l'intérieur du répertoire $HOME des utilisateurs.


!!! Note 

    Si des données uploadées doivent être accédées par un autre conteneur, la stratégie consiste à externaliser /home sur un volume commun, et à donner les permissions à l'autre conteneur en alignant le GID. Pour permettre cela, le MODE et le UMASK du répertoire d'upload sont paramétrables :

- SSHD_SFTP_UPLOAD_MODE (2770) : permissions du répertoire d'upload

- SSHD_SFTP_UPLOAD_UMASK (007) : umask par défaut du répertoire d'upload

- SSHD_FIX_HOME_PERMISSIONS (true) : si cette variable est passée à autre chose que **true**, alors l'entrypoint ne tente pas de modification de permission ni de propriétaire du contenu de /home (problématique sur montage NFS avec root_squash)



### Variables dans le cas d'utilisateur unique

- SSHD_USER_NAME (user) : nom de l'utilisateur autorisé à se connecter. Cette variable est ignorée dans le cas où un fichier user-accounts **est** fourni.

- SSHD_AUTHORIZED_PUBLIC_KEYS (empty) : clé(s) publique(s) à accepter (fichier .ssh/authorized_keys du USER). Concerne uniquement l'utilisateur SSHD_USER_NAME, et uniquement si un fichier user-accounts **n'est pas** fourni.

- SSHD_USER_PASSWORD (empty) : mot de passe de cet utilisateur.  **Attention** : mieux vaut utiliser un SECRET pour passer ce mot de passe au conteneur.

- SSHD_SECRET_KEY : nom du docker-secret contenant le mot de passe de l'utilisateur. Autant que possible, éviter d'utiliser la variable SSHD_USER_PASSWORD en production, et préférer l'utilisation d'un secret.

- SSHD_ENABLE_SUDO : Déclare l'utilisateur défini en tant que sudoer. Uniquement disponible si un seul utilisateur est créé. Variable ignorée dans le cas d'utilisateurs multiples créés avec un fichier "user-accounts".



## Authentification

Si une clé publique est passée, alors c'est authentification par clé publique.

Sinon, si un mot de passe est fourni, ce sera authentification par mot de passe :

- d'abord par le secret s'il existe un secret (voir variable SSHD_SECRET_KEY),

- à défaut par la variable d'environnement SSHD_USER_PASSWORD.

Si aucun des 3 n'est fourni, le conteneur s'arrête avec un message d'erreur.


## Réglages & sécurité

On peut activer le droit de sudo pour l'utilisateur (par défaut ce n'est pas actif).

On peut restreindre l'accès à SFTP (pas de shell dans ce cas là).


### Volumes

On a intérêt bien sûr à monter un volume de données pour /home.

Le répertoire de base de l'utilisateur est /home/user-name.


## Changelog

### 2023/05/30 (23.150), alpine 3.18


### 2022/02/01 (22.32), alpine 3.15
 - Ajout de la variable **SSHD_SFTP_ENABLE** : pour activer SFTP (sans forcément le chrooter)

### 2022/01/11 (22.11), alpine 3.15
- CI: rules/build : sur modif de /ep.d/*
- Arrêt de l'EP en cas d'erreur sur un des scripts appelés.

### 2022/01/10 (22.10), alpine 3.15
- Ajout de tests et suppression des users & goupes en conflit avev noms / UID / GID demandés

### 2021/09/18 (21.259), alpine 3.14
- Add changelog to readme.md
- Add Healchcheck
- CI: split build/push
- CI: add rules to stages
- Port d'écoute paramétrable par variable `SSHD_PORT`
- ajout d'infos complémentaires au démarrage si `SSHD_DEBUG_EP` vaut true

