#!/bin/bash

# Préparation Supervisord
mkdir -p /etc/supervisord.d

echo "
[supervisord]
user=root
pidfile=/var/run/supervisord.pid
logfile=/var/log/supervisord.log
loglevel=info
nodaemon=true

[include]
files = /etc/supervisord.d/*.ini
" > /etc/supervisord.conf

# Export des variables d'environnement pour les scripts d'EP
for V in $(set | grep ^SSHD_); do
   export ${V//=*}
done

# Prise en compte DES scripts d'EP.
for S in /ep.d/*.sh; do
   if [ -x "${S}" ] ; then 
      echo "Running : ${S} (in a sub-shell)"
      "${S}"
   else
      echo "Sourcing : ${S}"
      . "${S}"
   fi
   # Stop si erreur
   [ $? -ne 0 ] && echo "Exiting." && exit 1
done

[ ! -f /etc/ssh/sshd_config ] && echo "ERREUR : Fichier /etc/ssh/sshd_config manquant." && exit 1
[ "${SSHD_DEBUG_EP=-false}" == "true" ] && {
   echo -e "\nConfig SSHD: "
   cat -n /etc/ssh/sshd_config
   echo -e "===============================\n"
}

[ ! -f /etc/supervisord.d/sshd.ini ] && echo "ERREUR : Fichier supervisord.d/sshd.ini manquant." && exit 1
[ "${SSHD_DEBUG_EP=-false}" == "true" ] && {
   echo -e "\nConfig Supervisord-SSH: "
   cat -n /etc/supervisord.d/sshd.ini
   echo -e "===============================\n"
}

# check /home permissions
[ "${SSHD_DEBUG_EP=-false}" == "true" ] && {
   echo -e "\n/home permissions: "
   find /home -maxdepth 2 -exec ls -ld {} \;
   echo -e "===============================\n"
}

# Gestion du MAX_LIFETIME
if [ "${SSHD_MAX_LIFETIME:-0}" != 0 ] ; then 
   WRAPPER="timeout ${SSHD_MAX_LIFETIME}"
fi

# Exec "CMD" : 
# - si tout s'est bien passé jusqu'à ici 
# - au travers de timeout si demandé
[ $? -eq 0 ] && exec ${WRAPPER} "$@"
