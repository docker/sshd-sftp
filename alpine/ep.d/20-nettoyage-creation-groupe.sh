#!/bin/bash

# Debug mode ?
[ "${SSHD_DEBUG_EP=-false}" == "true" ] && set -x

# Identité
_UID=${SSHD_USER_UID:=1000}
_GID=${SSHD_USER_GID:=1000}
_USER=${SSHD_USER_NAME:=user}
_GROUP=${SSHD_GROUP_NAME:=scpusers}

# ==================================================
# Nettoyage de l'utilisateur portant notre UID cible
# ==================================================
L=$(getent passwd ${_UID}) ; [ $? = 0 ] && echo "Removing user ${L//:*} (UID ${_UID} conflict)"  && userdel -r ${L//:*}

# Tests d'existance du _USER et nettoyage si nécessaire
if grep -r ^${_USER} /etc/passwd ; then 
   echo "Suppression de l'utilisateur ${_USER} préexistant" 
   userdel -r ${_USER}
fi

# =================================================================
# Nettoyage du groupe portant notre GID cible, et création du nôtre
# =================================================================
L=$(getent group ${_GID} ) ; [ $? = 0 ] && echo "Removing group ${L//:*} (GID ${_GID} conflict)" && groupdel   ${L//:*}

# Nettoyage & Création du groupe
if grep -r ^${_GROUP} /etc/group ; then 
   echo "Suppression du groupe ${_GROUP} préexistant" 
   groupdel ${_GROUP}
fi
# Le groupe demandé est toujours créé
groupadd -f -g ${_GID} ${_GROUP} || { echo "Err: groupadd ${_GROUP}" ; exit 1; }


echo "$0: done !"