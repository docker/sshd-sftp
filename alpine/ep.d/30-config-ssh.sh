#!/bin/bash

# Debug mode ?
[ "${SSHD_DEBUG_EP=-false}" == "true" ] && set -x


# Config SSHD
echo "
Port ${SSHD_PORT:-22}
Protocol 2
HostKey /etc/ssh/ssh_host_ed25519_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_rsa_key

UseDNS no

PermitRootLogin no
X11Forwarding no
AllowTcpForwarding no
LogLevel ${SSHD_LOGLEVEL:-info}
StrictModes ${SSHD_STRICT_MODES:-yes}

" > /etc/ssh/sshd_config

# On active SFTP sans chroot ni restriction ?
if [ "${SSHD_SFTP_ENABLE:=false}" == true ] ; then

  echo -e "Subsystem sftp /usr/lib/ssh/sftp-server" >> /etc/ssh/sshd_config

else

  # SFTP seulement ? si oui, on chroote.
  if [ "${SSHD_SFTP_ONLY:=false}" == true ] ; then

    if [ "${SSHD_SFTP_ONLY_CHROOT_DIRECTORY}" ] ; then
      echo -e "Subsystem sftp internal-sftp\nForceCommand internal-sftp -u ${SSHD_SFTP_UPLOAD_UMASK:-007}\nChrootDirectory ${SSHD_SFTP_ONLY_CHROOT_DIRECTORY}" >> /etc/ssh/sshd_config
    else
      echo -e "Subsystem sftp internal-sftp\nForceCommand internal-sftp -u ${SSHD_SFTP_UPLOAD_UMASK:-007}\nChrootDirectory %h" >> /etc/ssh/sshd_config
    fi

  fi

fi

# Clés SSH "host"
ssh-keygen -A
[ "${SSHD_DEBUG_EP=-false}" == "true" ] && {
    echo -e "\nhost-keys produites dans /etc/ssh :"
    ls -l /etc/ssh/ssh_host* | cat -n
    echo -e "===============================\n"
}

# Prepare SUPERVISOR program
echo "
[program:sshd]
command=/usr/sbin/sshd -D ${SSHD_DEBUG_SERVER_FLAGS:-} -e -E /proc/1/fd/2

" > /etc/supervisord.d/sshd.ini

echo "$0: done !"