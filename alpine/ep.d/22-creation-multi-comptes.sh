#!/bin/bash

# Debug mode ?
[ "${SSHD_DEBUG_EP=-false}" == "true" ] && set -x

# Identité
_UID=${SSHD_USER_UID:=1000}
_GID=${SSHD_USER_GID:=1000}
_USER=${SSHD_USER_NAME:=user}
_GROUP=${SSHD_GROUP_NAME:=scpusers}

# Init global vars utilisées dans tous les cas
_ACCOUNT_FNAME=${MULTIPLE_ACCOUNT_FILENAME:-/root/user-accounts}


# ===========================================================
# Si fichier "/root/user-accounts" absent -> ce n'est pas ici
# ===========================================================
if  [ ! -f "${_ACCOUNT_FNAME}" ]  
then
  echo "Pas de fichier ${MULTIPLE_ACCOUNT_FILENAME} !"
  exit
fi

# =======================================================================
# Présence d'un fichier de liste de comptes, seuls ces comptes sont créés
#   !!!SECURITE!!! ces comptes devraient être authentifiés par clé
# =======================================================================
#
# L: Login, P: Password (ou clé)
while read L P
do

    # Ligne en Commentaire, on passe ?
    [ "${L:0:1}" == "#" ] && continue

    # Login ou password vide, on passe ?
    ( [ "${L}" == "" ] || [ "${P}" == "" ] ) && continue
    
    # Tout le monde le même UID ?
    if [ "${SSHD_SAME_UID:-false}" == false ]; then
        UID_GID="-g ${_GID}"
    else
        UID_GID="-o -u ${_UID} -g ${_GID}"
    fi

    # création du compte et éventuellement de son répertoire Home si chroot demandé
    [ -z "${SSHD_SFTP_ONLY_CHROOT_DIRECTORY}" ] && [ ! -d /home/${L} ] && USERADD_MKDIR="-m"
   
    echo -n "==> useradd ${USERADD_MKDIR} -K MAIL_DIR=/dev/null ${UID_GID} ${L}... "
    useradd ${USERADD_MKDIR} -K MAIL_DIR=/dev/null ${UID_GID} ${L} || { echo "Err: useradd ${L}" ; exit 1; }
    echo "User '${L}' created." 

    # =========================================
    # Test si P est une clé publique ou un pass    
    # =========================================
    if [ ${P//:*} == "pubkey" ]; then
        # Installation de la clé publique pour ce user là
        if [ ! -d /home/${L}/.ssh ]; then
            install -d -m 700 /home/${L}/.ssh || { echo "Err: mkdir /home/${L}/.ssh failed. Check if /home/${L} exists !" ; exit 1; }
        fi

        echo "${P//*:}"        >> /home/${L}/.ssh/authorized_keys
        chmod 600                 /home/${L}/.ssh/authorized_keys
        chown -R ${L}             /home/${L}/.ssh

        # Déblocage du compte
        usermod -p pubkey-only ${L}

    else  # C'est un mot de passe
        echo "${L}:${P:-null}" | chpasswd
    fi

    # Un répertoire unique pour chroot SFTP ?
    #    si non, on doit maintenant créer le dossier d'upload perso
    #    si oui, il sera créé une fois pour toutes après tous les comptes utilisateur.
    if [ "${SSHD_SFTP_ONLY:=false}" == true ] && [ -z "${SSHD_SFTP_ONLY_CHROOT_DIRECTORY}" ] ; then

      if [ ! -z "${SSHD_SFTP_UPLOAD_DIRNAME}" ]
      then
          install -d -m ${SSHD_SFTP_UPLOAD_MODE:-2770} -o ${L} -g ${_GROUP} /home/${L}/${SSHD_SFTP_UPLOAD_DIRNAME} || { echo "Err: mkdir /home/${L}/${SSHD_SFTP_UPLOAD_DIRNAME} failed" ; exit 1; }
      fi

    fi

    unset USERADD_MKDIR

done < "${_ACCOUNT_FNAME}"
unset L P

# Cas d'un chroot global, on le crée à la fin, on a forcément un dossier d'upload à créer
if [ ! -z "${SSHD_SFTP_ONLY_CHROOT_DIRECTORY}" ] ; then

  install -d -m ${SSHD_SFTP_UPLOAD_MODE:-2770} -o ${_UID} -g ${_GID} ${SSHD_SFTP_ONLY_CHROOT_DIRECTORY} || { echo "Err: mkdir ${SSHD_SFTP_ONLY_CHROOT_DIRECTORY} failed" ; exit 1; }

  [ ! -d "${SSHD_SFTP_ONLY_CHROOT_DIRECTORY}" ] && echo "Erreur: le dossier ${SSHD_SFTP_ONLY_CHROOT_DIRECTORY} n'existe pas." && exit 1
  chown root.root ${SSHD_SFTP_ONLY_CHROOT_DIRECTORY}
  
  echo "Création du dossier d'upload : ${SSHD_SFTP_UPLOAD_DIRNAME:=upload}"
  install -d -m ${SSHD_SFTP_UPLOAD_MODE:-2770} -o ${_UID} -g ${_GID} ${SSHD_SFTP_ONLY_CHROOT_DIRECTORY}/${SSHD_SFTP_UPLOAD_DIRNAME} || { echo "Err: mkdir ${SSHD_SFTP_ONLY_CHROOT_DIRECTORY}/${SSHD_SFTP_UPLOAD_DIRNAME} failed" ; exit 1; }
  find   ${SSHD_SFTP_ONLY_CHROOT_DIRECTORY} -exec ls -lnd {} \;

fi


echo "$0: done !"