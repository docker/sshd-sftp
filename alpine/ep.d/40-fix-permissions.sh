#!/bin/bash

# Debug mode ?
[ "${SSHD_DEBUG_EP=-false}" == "true" ] && set -x


# SFTP ONLY 
if [ "${SSHD_SFTP_ONLY:=false}" == true ] && [ "${SSHD_FIX_HOME_PERMISSIONS:-true}" == true ]; then

  for D in /home/*; do
    [ ! -d $D ] && continue
    chown root.root $D || { echo "Err: chown root.root on $D failed" ; exit 1; }
    chmod 755       $D || { echo "Err: chmod 755 on $D failed" ; exit 1; }
  done
fi

echo "$0: done !"