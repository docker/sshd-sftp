#!/bin/bash

# Debug mode ?
[ "${SSHD_DEBUG_EP=-false}" == "true" ] && set -x


# Identité
_UID=${SSHD_USER_UID:=1000}
_GID=${SSHD_USER_GID:=1000}
_USER=${SSHD_USER_NAME:=user}
_GROUP=${SSHD_GROUP_NAME:=scpusers}

# Init global vars utilisées dans tous les cas
_ACCOUNT_FNAME=${MULTIPLE_ACCOUNT_FILENAME:-/root/user-accounts}

# Création du ou des comptes

# Si fichier "/root/user-accounts" présent -> ce n'est pas ici
# ============================================================
                                # =============================================
if  [ -f "${_ACCOUNT_FNAME}" ]  # ======= Création de comptes multiples =======
                                # =============================================
then
   echo "/!\ nothing to do, multiple account required"
   exit
fi

# ==================================================================
# ======= Création d'un unique compte, éventuellement sudoer =======
# ==================================================================
echo -n "==== useradd -K MAIL_DIR=/dev/null -u ${_UID} -g ${_GID} ${_USER}... "
useradd -K MAIL_DIR=/dev/null -u ${_UID} -g ${_GID} ${_USER} && echo "User '${_USER}', in group '${_GROUP}' created (uid=${_UID}, gid=${_GID})." || { echo "Err: useradd ${_USER}" ; exit 1; }

# On ne touhe pas au Home Driectory s'il est déjà est là. 
# Dans le cas contraire, on le crée avec des permisisons fermées
if [ ! -d /home/${_USER} ] ; then 
  mkdir /home/${_USER} || { echo "Err: mkdir /home/${_USER} failed" ; exit 1; }
  chown ${_USER}:${_GROUP} /home/${_USER} || { echo "Err: chown on /home/${_USER} failed" ; exit 1; }
  chmod go-w /home/${_USER} || { echo "Err: chmod on /home/${_USER} failed" ; exit 1; }
fi

# Ajout de la règle dans sudo
[ "${SSHD_ENABLE_SUDO}" == "true" ] && echo "${_USER} ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/${_USER}

# Si on présente une clé publique en variable, on l'ajoute en fin de fichier "authorized_keys2"
# Si un mot de passe est défini, alors authentification par mot de passe
if [ -n "${SSHD_AUTHORIZED_PUBLIC_KEYS}" ]; then

    # Validation des permissions d'accès à .ssh/authorized_keys
    if [ ! -d /home/${_USER}/.ssh ] ; then
        mkdir -m 700 /home/${_USER}/.ssh || { echo "Err: mkdir /home/${_USER}/.ssh" ; exit 1; }
    fi

    echo "${SSHD_AUTHORIZED_PUBLIC_KEYS}" >> /home/${_USER}/.ssh/authorized_keys
    chmod 600                                /home/${_USER}/.ssh/authorized_keys
    chown -R ${_USER}.${_GROUP}              /home/${_USER}/.ssh
    echo "SSHD_AUTHORIZED_PUBLIC_KEYS installed"

    # Déblocage du compte (locked par défaut), mais affectation d'un mot de passe "aléatoire"
    _TMPPASS=$( echo $(hostname)$(date) |mkpasswd -P 0 | cut -c32-48)
    echo "${_USER}:${_TMPPASS}" | chpasswd
    # echo "P=${_TMPPASS}"
    unset _TMPPASS

elif [ -f "/run/secrets/${SSHD_SECRET_KEY:=SSHD_USER_PASSWORD_SECRET_NAME}" ] ; then

    echo ${_USER}:$(cat /run/secrets/${SSHD_SECRET_KEY}) | chpasswd
    echo "User password updated"

elif [ ! -z "${SSHD_USER_PASSWORD}" ] ; then

    echo "Warning: Password provided via environment variable, this is security risk."
    echo ${_USER}:${SSHD_USER_PASSWORD} | chpasswd
    echo "User password updated"

else

    echo "Err: No SSHD_AUTHORIZED_PUBLIC_KEYS and no SSHD_USER_PASSWORD given."
    exit 1
  
fi




# SFTP ONLY ? Force sftp and chroot jail
if [ "${SSHD_SFTP_ONLY:=false}" == true ] ; then

  # Création du répertoire d'upload si demandé
  if [ ! -z "${SSHD_SFTP_UPLOAD_DIRNAME}" ]; then
    if [ ! -d /home/${_USER}/${SSHD_SFTP_UPLOAD_DIRNAME:-upload} ]; then
        mkdir -m ${SSHD_SFTP_UPLOAD_MODE:-2770} /home/${_USER}/${SSHD_SFTP_UPLOAD_DIRNAME:-upload} || { echo "Err: mkdir /home/${_USER}/${SSHD_SFTP_UPLOAD_DIRNAME:-upload} failed" ; exit 1; }
        chown ${_USER}:${_GROUP}                /home/${_USER}/${SSHD_SFTP_UPLOAD_DIRNAME:-upload} || { echo "Err: chown on /home/${_USER}/${SSHD_SFTP_UPLOAD_DIRNAME:-upload} failed" ; exit 1; }
    fi
  fi
fi



echo "$0: done !"